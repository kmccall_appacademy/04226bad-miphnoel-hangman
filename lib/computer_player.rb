
class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary = ComputerPlayer.default_dictionary)
    @dictionary = dictionary
    @length = nil
    @candidate_words = dictionary.dup
  end

  def self.default_dictionary
    File.readlines('dictionary.txt').map(&:chomp)
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def check_guess(guessed_letter)
    correct_indices = []

    secret_word.each_char.with_index do |ch, idx|
      correct_indices << idx if ch == guessed_letter
    end

    correct_indices
  end

  def guess(board)
    puts "\n____________________"
    puts "\nYour secret word: #{board}"
    i = 0
    loop do
      most_frequent_ltr = most_frequent_letters[i][0]
      return most_frequent_ltr unless board.include?(most_frequent_ltr)
      i += 1
    end
  end

  def register_secret_length(length)
    @length = length
    @candidate_words = candidate_words.select do |word|
      word.length == length
    end
  end

  def handle_response(guess, indices)
    if indices.empty?
      @candidate_words = candidate_words.reject do |word|
        word.include?(guess)
      end
    else
      @candidate_words = candidate_words.select do |word|
        indices.all? { |idx| word[idx] == guess } &&
          word.count(guess) == indices.size
      end
    end
    raise UnknownWordError if candidate_words.empty?
  end

  def most_frequent_letters
    counts = Hash.new(0)

    candidate_words.each do |word|
      word.each_char do |ch|
        counts[ch] += 1
      end
    end

    counts.sort_by { |_letter, count| count }.reverse
  end
end

class UnknownWordError < StandardError

end
