require_relative 'human_player'
require_relative 'computer_player'
# require 'pry'
require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board, :lives

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = '_'
    @lives = 6
  end

  def play
    intro
    setup
    take_turn until over?
    outro
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)

    @board = '_' * length
  end

  def take_turn
    guess = guesser.guess(board)
    correct_indices = referee.check_guess(guess)
    # byebug
    update_board(guess, correct_indices)
    guesser.handle_response(guess, correct_indices)
    puts "Lives remaining: #{lives}"
  rescue UnknownWordError
    puts "I don't think I know your secret word. Did you lie? No? Alright, you win this time."
    play_again?
  end

  def update_board(guess, indices)
    indices.each do |index|
      @board[index] = guess
    end

    @lives -= 1 if indices.empty?
  end

  def over?
    !board.include?('_') || lives.zero?
  end

  def intro
    puts "It's time for Hangman!"
    if guesser.is_a?(HumanPlayer)
      puts "I'll pick a word, and you get to guess."
      puts "If you can spell the secret word before giving 6 wrong guesses, you win!\n\n"
    else
      puts "You pick a word, and I will try to guess it."
      puts "If I can't guess your word before giving 6 wrong guesses, you win!\n\n"
    end
  end

  def outro
    puts "\n************"
    if lives.zero?
      puts "\nThe hangman got his man nice and hanged. No guesses remaining."
      if referee.is_a?(ComputerPlayer)
        puts "The secret word was '#{referee.secret_word}'"
      end
    else
      puts "\nYes! Cut that man down! The secret word has been found!"
      puts "The secret word was '#{board}'"
    end

    play_again?
  end

  def play_again?
    puts "\nPlay again? (y/n)"
    if gets.chomp.downcase == 'y'
      @lives = 6
      guesser.letters_guessed = [] if guesser.is_a?(HumanPlayer)
      play
    end

    puts "See ya later!"
    exit
  end
end

if __FILE__ == $PROGRAM_NAME
  mike = HumanPlayer.new
  comp = ComputerPlayer.new

  players = {
    guesser: mike,
    referee: comp
  }

  game = Hangman.new(players)
  game.play
end
