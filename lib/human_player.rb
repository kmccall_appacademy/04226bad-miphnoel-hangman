
class HumanPlayer
  attr_accessor :letters_guessed

  def initialize
    @letters_guessed = []
  end

  def pick_secret_word
    puts "\nHow many total letters are in your secret word? (ex. '6')"
    gets.chomp.to_i
  rescue
    puts "Please input length in the form of a number"
    pick_secret_word
  end

  def register_secret_length(length)
    puts "The secret word is #{length} letters long."
  end

  def guess(board)
    puts "\n________________________"
    puts "\nSecret word: #{board}"
    puts "\nLetters guessed: [#{letters_guessed.join(', ')}]"
    puts "\nWhat is your guess? (ex. 'a')"
    guess = gets.chomp.downcase
    @letters_guessed << guess unless letters_guessed.include?(guess)
    sleep(1)
    return guess if ("a".."z").cover?(guess) && guess.size == 1

    puts "Please enter a single letter guess"
    guess(board)
  end

  def check_guess(guess)
    puts "\nIs there a(n) '#{guess}' in your word? (y/n)"
    return [] if gets.chomp.downcase == 'n'

    puts "\nAt which indices? (ex. '0, 2, 4')"
    gets.chomp.split(', ').map(&:to_i)
  end

  def handle_response(guess, indices)
    if indices.empty?
      puts "Sorry, no '#{guess}'s"
    elsif indices.size == 1
      puts "Yep, there is 1 #{guess}"
    else
      puts "Yep, there are #{indices.size} '#{guess}'s"
    end
  end
end
